<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html>
<head>
	<title>Mapa de paradas</title>
	
</head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Geolocalizador de playas - España</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#">Acerca de</a></li>              
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    <!-- Mapa -->
    <div id="mapa"></div>
    
    <!-- Scripts -->
     <script src="/resources/js/map.js"> </script> 
    <script async defer
		src="https://maps.googleapis.com/maps/api/js?callback=initMap&key=AIzaSyDBvPHHQqByzEwRmgyQy0TRUSHrq8H3PII">
    </script>
    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>

    <script src="/resources/js/bootstrap.min.js"> </script>   
    
    <link  rel="stylesheet" type="text/css" href="/resources/css/bootstrap-theme.min.css"/> 
    <link  rel="stylesheet" type="text/css" href="/resources/css/bootstrap.min.css"/> 
    <link  rel="stylesheet" type="text/css" href="/resources/css/style.css"/> 
   
    <script src="/resources/js/playas.js"> </script> 
</body>
</html>
