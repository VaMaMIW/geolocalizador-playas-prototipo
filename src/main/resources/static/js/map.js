/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var map;
var markersArray = [];
var overlayPluviosidad;
var overlayPlayas;
var botonAniadirCapaPlayas;
var botonAniadirCapaPluviosidad;
var marcaRuta = null;
var playas;
var directions;
var visualizadorRuta;
var posSelect = {
    'lat': '',
    'lng': ''
};
var region = {
    'minX': '',
    'minY': '',
    'maxX': '',
    'maxY': ''
}

function getTileUrlPlayas(coord, zoom) {
    var proj = map.getProjection();
    var zfactor = Math.pow(2, zoom);
    var esquinaSuperior = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
    var esquinaInferior = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
    var bbox = esquinaSuperior.lng() + "," + esquinaInferior.lat() + "," + esquinaInferior.lng() + "," + esquinaSuperior.lat();

    var WMS_URL = 'http://wms.magrama.es/sig/Costas/Playas/wms.aspx?';
    var myURL = WMS_URL + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&SRS=EPSG%3A4326&WIDTH=256&HEIGHT=256&FORMAT=image/png&TRANSPARENT=TRUE&STYLES=default";
    var WMS_Layers = 'Guía%20de%20Playas%20de%20España';
    myURL += "&LAYERS=" + WMS_Layers;
    myURL += "&BBOX=" + bbox;
    return myURL;

}
function getTileUrlPluviosidad(coord, zoom) {
    var proj = map.getProjection();
    var zfactor = Math.pow(2, zoom);
    var esquinaSuperior = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
    var esquinaInferior = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
    var bbox = esquinaSuperior.lng() + "," + esquinaInferior.lat() + "," + esquinaInferior.lng() + "," + esquinaSuperior.lat();

    return 'http://www.opengis.uab.es/cgi-bin/iberia/Miramon5_0.cgi?'
            + 'request=GetMap'
            + '&service=WMS&version=1.1.1'
            + '&layers=clima_anual_iberia'
            + '&style=Pluviometria'
            + '&srs=EPSG%3A4258'
            + '&bbox=' + bbox
            + '&width=256&height=256'
            + '&format=image%2Fpng';
}

function onClickBotonCapaPluviosidad() {
    var posicionCapa = -1;
    map.overlayMapTypes.forEach(function (capa, indice) {
        if (capa === overlayPluviosidad) {
            posicionCapa = indice;
        }
    });
    if (posicionCapa === -1) {
        map.overlayMapTypes.push(overlayPluviosidad);
        botonAniadirCapaPluviosidad.innerHTML = 'Ocultar temperatura';
    } else {
        map.overlayMapTypes.removeAt(posicionCapa);
        botonAniadirCapaPluviosidad.innerHTML = 'Mostrar temperatura';
    }
}
function onClickBotonCapaPlayas() {
    var posicionCapa = -1;
    map.overlayMapTypes.forEach(function (capa, indice) {
        if (capa === overlayPlayas) {
            posicionCapa = indice;
        }

    });
    if (posicionCapa === -1) {
        map.overlayMapTypes.push(overlayPlayas);
        botonAniadirCapaPlayas.innerHTML = 'Ocultar playas';
    } else {
        map.overlayMapTypes.removeAt(posicionCapa);
        botonAniadirCapaPlayas.innerHTML = 'Mostrar playas';
    }


}

function crearBotonCapaPluviosidad() {
    botonAniadirCapaPluviosidad = document.createElement('button');
    botonAniadirCapaPluviosidad.innerHTML = 'Mostrar temperatura';
    botonAniadirCapaPluviosidad.className = 'btn btn-info';
    botonAniadirCapaPluviosidad.setAttribute('style', 'margin-top: 9px;');
    botonAniadirCapaPluviosidad.addEventListener('click', onClickBotonCapaPluviosidad);
    return botonAniadirCapaPluviosidad;
}

function crearBotonCapaPlayas() {
    botonAniadirCapaPlayas = document.createElement('button');
    botonAniadirCapaPlayas.innerHTML = 'Mostrar playas';
    botonAniadirCapaPlayas.className = 'btn btn-info';
    botonAniadirCapaPlayas.setAttribute('style', 'margin-top: 9px;     margin-left: 10px;');
    botonAniadirCapaPlayas.addEventListener('click', onClickBotonCapaPlayas);
    return botonAniadirCapaPlayas;
}

function onRutaCalculada(resultado, estado) {
    if (estado === 'OK') {
        console.log(resultado);
        alert(resultado.routes[0].legs[0].distance.text +
                ' ' + resultado.routes[0].legs[0].duration.text);
        visualizadorRuta.setDirections(resultado);
    }
}

function onClickEnMapa(evento) {
    addMarker(evento.latLng);
}
// Aniade un marcador al mapa, posicion de usuario
function addMarker(location) {

    posSelect.lat = location.lat();
    posSelect.lng = location.lng();

    var marker = new google.maps.Marker({
        position: location,
        map: map,
        title: 'Tu origen'
    });

    var contentString = '<div class="text-center">'
            + '<img class="img-form" src="/resources/img/ways-form.png">'
            + '<div style="margin-top: 20px; font-size: 14px;  " >'
            + '<div style="display: inline-block;"><p>distancia del desplazamiento </p></div>'
            + '<div style="display: inline-block; margin-left: 7px; " class="form-group">'
            + '<input style=" border-radius: 4px;     width: 100px;" placeholder="Km a recorrer" id="distance-input" name="distancia">'
            + '</div>'
            + '</div>'
            + '<button class="btn btn-default" onClick="calcularRegion()">Buscar rutas</button>'
            + '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString,
        maxWidth: 300
    });

    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
    infowindow.open(map, marker);
    //reinicia los marcadores
    clearOverlays();
    markersArray.push(marker);
}

// Elimina los marcadores del mapa; pero los mantiene en el array
function clearOverlays() {
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
    }
}

function initMap() {

    //centro de espania
    var centro = {lat: 40.0000000, lng: -4.0000000};

    map = new google.maps.Map(document.getElementById('mapa'), {
        zoom: 6,
        center: centro
    });

    map.addListener('click', onClickEnMapa);

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(crearBotonCapaPluviosidad());
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(crearBotonCapaPlayas());

    overlayPluviosidad = new google.maps.ImageMapType({
        tileSize: new google.maps.Size(256, 256),
        getTileUrl: getTileUrlPluviosidad
    });

    overlayPlayas = new google.maps.ImageMapType({
        tileSize: new google.maps.Size(256, 256),
        getTileUrl: getTileUrlPlayas
    });

    visualizadorRuta = new google.maps.DirectionsRenderer({
        map: map
    });
}

function calcularRegion() {
    var latitud = posSelect.lat;
    var longitud = posSelect.lng;
    var distanceKilometres = document.getElementById('distance-input').value;

    var radiusEarthKilometres = 6371.01;
    var initialBearingRadians = degreesToRadians(315);
    var distRatio = distanceKilometres / radiusEarthKilometres;
    var distRatioSine = Math.sin(distRatio);
    var distRatioCosine = Math.cos(distRatio);

    var startLatRad = degreesToRadians(latitud);
    var startLonRad = degreesToRadians(longitud);

    var startLatCos = Math.cos(startLatRad);
    var startLatSin = Math.sin(startLatRad);

    var endLatRads = Math.asin((startLatSin * distRatioCosine) + (startLatCos * distRatioSine * Math.cos(initialBearingRadians)));

    var endLonRads = startLonRad
            + Math.atan2(
                    Math.sin(initialBearingRadians) * distRatioSine * startLatCos,
                    distRatioCosine - startLatSin * Math.sin(endLatRads));

    var endPoint = {
        'lat': radiansToDegrees(endLatRads),
        'lng': radiansToDegrees(endLonRads)
    };

    //calculo de la region
    region.minY = String(latitud - (endPoint.lat - latitud));
    region.maxY = String(endPoint.lat);
    region.minX = String(endPoint.lng);
    region.maxX = String(longitud + (longitud - endPoint.lng));

    var url = window.location;
    
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url + "playas/region",
        data: JSON.stringify(region),
        dataType: 'json',
        success: function (result) {
               playas = result.items;
               if(playas!==null){
                    directions = new Array(playas.length);
                }else{
                    directions = new Array(0);
                }
               
               var panelPlayas = panelPlayasDescubiertas(result);
               
               map.controls[google.maps.ControlPosition.LEFT_CENTER].clear();              
               map.controls[google.maps.ControlPosition.LEFT_CENTER].push(panelPlayas);
               
               if(playas !== null){  
                   if(playas.length > 0){
                    calculateRoute(0, posSelect);
               

                for(var j = 0 ; j < playas.length; j++){
                    var element = playas[j];

                    var auxPlaya =  new google.maps.LatLng(element.coordenadaY, element.coordenadaX);  
                     var marker1 = new google.maps.Marker({
                         position: auxPlaya,
                         map: map,
                         icon: '/resources/img/icon_info.png',
                         title: element.nombre
                     });

                     
                     contentString = "";
                    
                     marker1.addListener('click', function () {
                         var infowindowPlaya = new google.maps.InfoWindow();
                         var contentString =                            
                              '<div css="info-window-playa" style=" font-size: 14px;  " >'
                                 + '<h1 style="font-size: 14px; ">' +  element.nombre + '</h1>'
                                 + '<p>'+element.descripcion +'</p>'
                                 + '<a href="'+element.web+'">Web información </a>'
                             + '</div>' ;                         
                             
                         
                         infowindowPlaya.setContent( contentString );
                         infowindowPlaya.open(map, this);
                     });
                   
                     
                 }
            }
        }
        },
        error: function (e) {
            debugger;
            console.log("ERROR: ", e);
        }
    });
}

function panelPlayasDescubiertas(result){
    if(result.items !== null ){
            desplegablePlayas = document.createElement('div');    
            desplegablePlayas.setAttribute('style', 'min-width: 220px;') ;         
            var contentHtml = '<button type="button" style="min-width: 220px;" class="btn btn-info" data-toggle="collapse" data-target="#demo">Playas descubiertas</button>'
                +'<div id="demo" style="overflow-y: scroll; height:150px; background-color: #31B0D5; padding-left: 30px; border-radius: 5px;  padding-bottom: 10px;" class="collapse in">' ;
                var radio;
                for(var i = 0; i < result.items.length ; i++){
                    radio =  '<br/>';  
                        if(i === 0){
                            radio +='  <input type="checkbox"  name="playas-d" id="playas-d" value="'+i+'" onclick="changeRoute(this)" checked>' 
                        }else{
                           radio += '  <input type="checkbox"  name="playas-d" id="playas-d" onclick="changeRoute(this)" value="'+i+'">'
                        }
                        radio += result.items[i].nombre;                           

                      contentHtml += radio;
                } 

                +'</div>'                   
                +'</div>';

          desplegablePlayas.innerHTML = contentHtml;
                      return desplegablePlayas;
        }else{
            alert('No se encontraron resultados');
        }
    }
    function calculateRoute(destinoIndex, posOrigen){

        var positionPlaya = new google.maps.LatLng(playas[destinoIndex].coordenadaY, playas[destinoIndex].coordenadaX);            
        var positionUser = new google.maps.LatLng(posOrigen.lat, posOrigen.lng);
        var camino = {
            'origen' : positionUser,
            'destino' : positionPlaya
          };
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = directions[destinoIndex];
        if (directionsDisplay !== false) {
            directionsDisplay = new google.maps.DirectionsRenderer;
        }
        directionsDisplay.setMap(map);
        directions[destinoIndex] = directionsDisplay;
        directionsService.route(
        {
            origin: camino.origen,
            destination: camino.destino,
            travelMode: google.maps.TravelMode.DRIVING,
            optimizeWaypoints: true
        },
        function (response, status) {
            debugger;
            if (status === google.maps.DirectionsStatus.OK) {

                directionsDisplay.setDirections(response);

            } else {

               alert('Problema en el servicio de geolocalizacion');
            }
        });
    }
    function changeRoute(checkbox){
        var i = checkbox.value;
        var directionDisplay = directions[i];
        if (checkbox.checked) {
            if (directionDisplay !== false) {
                directionDisplay = new google.maps.DirectionsRenderer;
            }
            directionDisplay.setMap(map);
            calculateRoute(i, posSelect);
        } else {
            directionDisplay.setMap(null);

            directionDisplay.setOptions( { 'suppressMarkers' : true } );
            directionDisplay = null;
        }
        directions[i]= directionDisplay;
    
    
}
function degreesToRadians(degrees) {
    var degToRadFactor = Math.PI / 180;
    return degrees * degToRadFactor;
}

function radiansToDegrees(radians) {
    var radToDegFactor = 180 / Math.PI;
    return radians * radToDegFactor;

}
