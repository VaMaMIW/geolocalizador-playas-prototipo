package com.miw.planificadorvacaciones.service;

import com.miw.planificadorvacaciones.model.Playa;
import com.miw.planificadorvacaciones.model.Region;
import java.util.List;

/**
 *
 * @author MariaJose
 */
public interface PlayaService {
    
    public List<Playa> findPlayas();

    public List<Playa> findPlayasByRegion(Region region);
}
