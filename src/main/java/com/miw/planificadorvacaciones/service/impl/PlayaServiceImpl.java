package com.miw.planificadorvacaciones.service.impl;

import com.miw.planificadorvacaciones.model.Playa;
import com.miw.planificadorvacaciones.model.Region;
import com.miw.planificadorvacaciones.persistence.PlayasDao;
import com.miw.planificadorvacaciones.service.PlayaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MariaJose
 */
@Service
public class PlayaServiceImpl implements PlayaService{
        
        
    @Autowired
    private PlayasDao playaDao;
        
    @Override
    public List<Playa> findPlayas() {
        return playaDao.findPlayas();
    }

    @Override
    public List<Playa> findPlayasByRegion(Region region) {
       
         List<Playa> playas =  playaDao.findPlayasByRegion(region);
        
         return playas;
    }
    
}
