/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miw.planificadorvacaciones.persistence;

import com.miw.planificadorvacaciones.model.Playa;
import com.miw.planificadorvacaciones.model.Region;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MariaJose
 */
@Repository
public class PlayasDao {
    
    
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public PlayasDao(JdbcTemplate jdbcTemplate) {		
		this.jdbcTemplate = jdbcTemplate;
	}
    
    public List<Playa> findPlayas(){
        List<Playa> playas = new ArrayList<>();
        
        jdbcTemplate.query(
                "SELECT Identificador, Nombre, Descripcion, Coordenada_X, Coordenada_Y, Web  FROM playas",
                (rs, rowNum) -> new Playa(rs.getLong("identificador"), rs.getString("nombre"),
                        rs.getString("descripcion"),
                        rs.getBigDecimal("Coordenada_X"), rs.getBigDecimal("Coordenada_Y"), rs.getString("Web"))
        ).forEach(playa -> playas.add(playa));
        
        return playas;
    }

    public List<Playa> findPlayasByRegion(Region region) {
        
          List<Playa> playas = new ArrayList<>();
        
        
        jdbcTemplate.query(
                "SELECT Identificador, Nombre, Descripcion, Coordenada_X, Coordenada_Y, Web FROM playas "
                        + "WHERE Coordenada_X <= ? AND Coordenada_X >= ? AND "
                        + "Coordenada_Y <= ? AND Coordenada_Y >= ?", 
                    new Object[]{ new BigDecimal(region.getMaxX()), new BigDecimal(region.getMinX()),
                        new BigDecimal(region.getMaxY()), new BigDecimal(region.getMinY())},
                (rs, rowNum) -> new Playa(rs.getLong("Identificador"), rs.getString("Nombre"),
                        rs.getString("Descripcion"), rs.getBigDecimal("Coordenada_X"), rs.getBigDecimal("Coordenada_Y"), rs.getString("Web"))
                    ).forEach(playa ->  {    playas.add(playa);   }
                );
        
        
        return playas;
    }
    
}
