/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miw.planificadorvacaciones.controller;


import com.miw.planificadorvacaciones.model.AjaxResponseBody;
import com.miw.planificadorvacaciones.model.Playa;
import com.miw.planificadorvacaciones.model.Region;
import com.miw.planificadorvacaciones.service.PlayaService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 *
 * @author Vane
 */
@Controller
@SpringBootApplication(scanBasePackages={"com.miw.planificadorvacaciones"})
public class PlanificadorViajesController {

    @Autowired
    private PlayaService playaService;

    public static void main(String[] args) {
        SpringApplication.run(PlanificadorViajesController.class, args);
    }

    @RequestMapping("/")
    public String index() {     
      
        return "index";
    }

    /**
     * Atiende peticion geolocalizacion de todas las playas - espania
     *
     * @return admin.comentarios
     */
    @RequestMapping(value = "/playas/todas", method = RequestMethod.POST)
    public ResponseEntity<?> listarPlayas(Errors errors) {

        AjaxResponseBody result = new AjaxResponseBody();

        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            result.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            return ResponseEntity.badRequest().body(result);

        }

        List<Playa> playas = playaService.findPlayas();

        if (playas.isEmpty()) {
            result.setMsg("no user found!");
        } else {
            result.setMsg("success");
        }
        result.setItems(playas);

        return ResponseEntity.ok(result);

    }
    @ResponseBody
    @RequestMapping(value = "/playas/region", method = RequestMethod.POST)
    public AjaxResponseBody getPlayasRegion(@RequestBody Region region,  Errors errors) {
       
        AjaxResponseBody response = new AjaxResponseBody();  
        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            response.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));
            
            return response;
        }
        try{
            List<Playa> playasRegion = playaService.findPlayasByRegion(region);
            response.setMsg("success");
            response.setItems(playasRegion);
           
        }catch(Exception e){
            response.setMsg("error "+e.getMessage());
            
        }
       
        return response;
    }

}
