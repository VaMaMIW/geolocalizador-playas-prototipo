/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miw.planificadorvacaciones.model;

import java.util.List;

/**
 *
 * @author MariaJose
 */
public class AjaxResponseBody {
    
    private String msg;
    private List<?> items;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<?> getItems() {
        return items;
    }

    public void setItems(List<?> items) {
        this.items = items;
    }
    
    
    
}
