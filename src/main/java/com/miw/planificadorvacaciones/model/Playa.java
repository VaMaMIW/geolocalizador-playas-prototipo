/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miw.planificadorvacaciones.model;

import java.math.BigDecimal;

/**
 *
 * @author MariaJose
 */
public class Playa {
    
    private Long identificador;
    private String nombre;
    private String descripcion;
    private BigDecimal coordenadaX;
    private BigDecimal coordenadaY;
    private String web;

    public Playa(Long identificador, String nombre, String descripcion,
            BigDecimal coordenadaX, BigDecimal coordenadaY, String web) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.coordenadaX = coordenadaX;
        this.coordenadaY = coordenadaY;
        this.web = web;
    }
   
    public Long getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Long identificador) {
        this.identificador = identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getCoordenadaX() {
        return coordenadaX;
    }

    public void setCoordenadaX(BigDecimal coordenadaX) {
        this.coordenadaX = coordenadaX;
    }

    public BigDecimal getCoordenadaY() {
        return coordenadaY;
    }

    public void setCoordenadaY(BigDecimal coordenadaY) {
        this.coordenadaY = coordenadaY;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getWeb() {
        return web;
    }
    

    @Override
    public String toString() {
        return "Playa{" + "identificador=" + identificador + ", nombre=" + nombre + ", descripcion=" + descripcion + ", coordenadaX=" + coordenadaX + ", coordenadaY=" + coordenadaY + '}';
    }
    
    
    
    
    
    
}
